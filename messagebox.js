
/**
 * The onclick event on the delete link
 */
function messagebox_delete_click(mid) {
  $.get(messagebox_url + '/delete/' + mid,
    function(contents) {
      $('#display-messagebox-table').empty().append(contents);
    }
  );
}

/**
 * The onclick event on the delete all link
 */
function messagebox_delete_all_click(question, arg) {
  if(confirm(question)) {
    $.get(messagebox_url + '/delete_all/' + arg,
      function(contents) {
        $('#display-messagebox-table').empty().append(contents);
      }
    );
  }
}

/**
 * The onclick event on the 'mark as read' link
 */
function messagebox_mark_as_read_click(mid) {
  $.get(messagebox_url + '/mark_as_read/' + mid,
    function(contents) {
      $('#display-messagebox-table').empty().append(contents);
    }
  );
}

/**
 * The onclick event on the 'mark all_as read' link
 */
function messagebox_mark_all_as_read_click() {
  $.get(messagebox_url + '/mark_all_as_read',
    function(contents) {
      $('#display-messagebox-table').empty().append(contents);
    }
  );
}

/**
 * The onclick event on the reply link
 */
function messagebox_reply_click(rid, username) {
  if($('#edit-messagebox-autocomplete-flag').val() == '1') {
    $('#edit-messagebox-user').val(username);
    $('#edit-messagebox-user')[0].focus();
  }
  else {
    var users_box = document.getElementById('edit-messagebox-rid');
    var n = users_box.length;

    for(var i = 0; i < n; i++) {
      if(users_box.options[i].value == rid) {
        users_box.selectedIndex = i;
        break;
      }
    }

    users_box.focus();
  }

  $('#edit-messagebox-message')[0].focus();
}

/**
 * The onclick event on the submit button
 */
function messagebox_submit_click() {
  var rid = -1;
  var username = '';

  if($('#edit-messagebox-autocomplete-flag').val() == '1') {
    username = $('#edit-messagebox-user').val();
  }
  else {
    var users_box = document.getElementById('edit-messagebox-rid');
    rid = users_box.options[users_box.selectedIndex].value;
  }

  var post_vars = {
    'rid' : rid,
    'username' : username,
    'message' : $('#edit-messagebox-message').val()
  };

  $.post(messagebox_url + '/send', post_vars,
    function(contents) {
      $('#messagebox-notify-message').empty().append(contents);
      $('#edit-messagebox-message').val('');
    }
  );

  return false;
}
